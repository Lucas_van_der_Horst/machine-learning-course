from abc import ABC, abstractmethod
from typing import List
from IPython.display import HTML


class Network(ABC):
    """The network interface should be the parent of PerceptronNetwork and NeuralNetwork"""
    @abstractmethod
    def get_weights(self) -> List[List[List[float]]]:
        """
        Should return the weights in the network,
        first dimension is the layer,
        second dimension is the perceptron / neuron,
        and the last dimension is the dendrite.

        ...I wonder if storing it as an multidimensional array would be more efficient anyway...
        ...and maybe use the dot product to process a layer...
        """
        pass

    @abstractmethod
    def feed_forward_steps(self, inputs: List[float]) -> List[List[float]]:
        """Feed forward but return the results on each step/layer"""
        pass

    def feed_forward(self, inputs: List[float]) -> List[float]:
        """Feeds the inputs through the network layer by layer and returns the output"""
        return self.feed_forward_steps(inputs)[-1]

    def to_svg(self, inputs: List[float], width: int = 500, height: int = 200, radius=20, stroke_width=3) -> HTML:
        """Display the network as a svg"""
        # This code became ugly faster than expected
        # https://www.pinterest.com/pin/202310208247211856

        weights = self.get_weights()
        feed_steps = self.feed_forward_steps(inputs)
        len_layer = lambda l: len(weights[l+1][0]) if l < len(weights)-1 else len(weights[l])
        horizontal_space = (width - 2*(radius+stroke_width)) / len(weights)
        vertical_space = (height - 2*(radius+stroke_width)) / (max([len_layer(l) for l in range(-1, len(weights))]) -1)
        circles = []
        path_points = []

        for l in range(-1, len(weights)):
            layer_path_points = []
            n_length = len_layer(l)
            for n in range(n_length):
                x = radius + stroke_width + (l+1) * horizontal_space
                y = (height - n_length * vertical_space) / 2 + (n+0.5) * vertical_space
                val = feed_steps[l+1][n]
                color = [str(val*255)] * 3
                circles.append(f'<circle cx="{x}" cy="{y}" r="{radius}" fill="rgb({", ".join(color)})"><title>{val}</title></circle>')
                layer_path_points.append((x+radius, y))
            path_points.append(layer_path_points)

        paths = []
        for l in range(len(path_points)-1):
            for i in range(len(path_points[l])):
                last = l >= len(path_points)-2
                for j in range(not last, len(path_points[l+1])):
                    weight = weights[l][j-1][i]
                    color = [min(max(-weight*255, 0), 255), min(max(weight*255, 0), 255), 0]
                    paths.append(f'<path d="m{path_points[l][i][0]} {path_points[l][i][1]} '
                                 f'{horizontal_space-2*radius} {path_points[l+1][j][1] - path_points[l][i][1]}" '
                                 f'stroke="rgb({", ".join(map(str, color))})"><title>{weight}</title></path>')

        return HTML(f"""
            <svg width="{width}" height="{height}" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g stroke="#000" stroke-width="{stroke_width}">
                    {enter.join(circles)}
                </g>
                <g fill="none" stroke="#000" stroke-width="{stroke_width}">
                    {enter.join(paths)}
                </g>
            </svg>
            """)


enter = '\n'
