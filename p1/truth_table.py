from IPython.display import Markdown
from itertools import product


def truth_table_perceptron(perceptron) -> Markdown:
    """Creates a truth table of the given perceptron and prints it"""
    n_inputs = len(perceptron.weights)
    output_string = f"|{'|'.join(map('in {}'.format, range(n_inputs)))}|out|\n|{'-|'*n_inputs}-|"
    for inputs in product((0, 1), repeat=n_inputs):
        output_string += f"\n|{'|'.join(map(str, inputs))}|{perceptron.get_output(inputs)}|"
    return Markdown(output_string)


def truth_table_network(network) -> Markdown:
    """Creates a truth table of the given network and prints it"""
    n_inputs = len(network.get_weights()[0][0])-1
    n_outputs = len(network.get_weights()[-1])
    output_string = f"|{'|'.join(map('in {}'.format, range(n_inputs)))}" \
                    f"|{'|'.join(map('out {}'.format, range(n_outputs)))}|" \
                    f"\n|{'-|'*(n_inputs+n_outputs)}"
    for inputs in product((0, 1), repeat=n_inputs):
        output_string += f"\n|{'|'.join(map(str, inputs))}|{'|'.join(map(str, network.feed_forward(list(inputs))[1:]))}|"

    return Markdown(output_string)
