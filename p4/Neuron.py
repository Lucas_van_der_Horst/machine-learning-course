from __future__ import annotations
import math
from typing import List


def sigmoid(x: float) -> float:
    return 1 / (1 + math.e ** (-x))


def deriv_sigmoid(x: float) -> float:
    """The derivative of the sigmoid"""
    return sigmoid(x) * (1 - sigmoid(x))


class Neuron:
    """Neuron unit"""
    weights: List[float]
    bias: float

    _last_inputs: List[float]
    _last_net_input: float  # net input
    _last_output: float
    _error: float

    def __init__(self, weights: List[float], bias: float):
        self.weights = weights
        self.bias = bias

    def calc_output(self, inputs: List[float]) -> float:
        """Calculate the output"""
        if len(inputs) != len(self.weights):
            raise ValueError(f"The inputs({inputs})-shape doesn't match the weights-shape: ({len(self.weights)},)")

        self._last_inputs = inputs
        self._last_net_input = sum([input_value * weight for input_value, weight in zip(inputs, self.weights)]) + self.bias
        self._last_output = sigmoid(self._last_net_input)
        return self._last_output

    def calc_error_output_neuron(self, target: float) -> float:
        """Calculate the error as this neuron is a output-neuron"""
        self._error = deriv_sigmoid(self._last_net_input) * -(target - self._last_output)
        return self._error

    def calc_error_hidden_neuron(self, next_neurons: List[Neuron], index_in_layer: int) -> float:
        """Calculate the error as this neuron is a hidden-neuron"""
        self._error = deriv_sigmoid(self._last_net_input) * sum([n._error * n.weights[index_in_layer] for n in next_neurons])
        return self._error

    def update(self, learning_rate: float):
        for i in range(len(self.weights)):
            self.weights[i] -= learning_rate * self._last_inputs[i] * self._error
        self.bias -= learning_rate * self._error

    def __str__(self) -> str:
        return f"<Perceptron weights: {self.weights} bias: {self.bias}>"

    def __repr__(self) -> str:
        return self.__str__()
