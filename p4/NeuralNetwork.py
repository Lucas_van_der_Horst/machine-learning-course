from Network import Network
from Neuron import Neuron
from typing import List
from time import time


class NeuronLayer:
    """A layer of neurons"""
    neurons: List[Neuron]

    def __init__(self, neurons: List[Neuron]):
        self.neurons = neurons


class NeuralNetwork(Network):
    """A network of neurons made out of fully connected layers"""
    neuron_layers: List[NeuronLayer]

    def __init__(self, neuron_layers: List[NeuronLayer]):
        self.neuron_layers = neuron_layers

    def get_weights(self) -> List[List[List[float]]]:
        return [[[n.bias] + n.weights for n in l.neurons] for l in self.neuron_layers]

    def feed_forward_steps(self, inputs: List[float]) -> List[List[float]]:
        """Passes the input values through the network layer by layer and returns the output vector/array"""
        working_steps = [[1.0] + inputs]
        for layer in self.neuron_layers:
            working_steps.append([1.0] + list(ner.calc_output(working_steps[-1][1:]) for ner in layer.neurons))
        return working_steps

    def backpropagate(self, target: List[float], learning_rate: float) -> None:
        """
        Applies backpropagation.
        Give the target of the last feed_forward run
        and a learning rate
        """
        # Calculating the error of each neuron
        # start with the output layer
        last_layer = self.neuron_layers[-1]
        for i in range(len(last_layer.neurons)):
            last_layer.neurons[i].calc_error_output_neuron(target[i])

        # backpropagate through each hidden layer
        for layer in self.neuron_layers[-2::-1]:
            for i in range(len(layer.neurons)):
                layer.neurons[i].calc_error_hidden_neuron(last_layer.neurons, i)
            last_layer = layer

        # Update each neuron
        for layer in self.neuron_layers:
            for neuron in layer.neurons:
                neuron.update(learning_rate)

    def train(self, inputs_set: List[List[float]], targets_set: List[List[float]], epochs: int, learning_rate: float):
        """Trains the neural network using backpropagation"""
        old_epoch_time = 0
        for epoch in range(1, epochs+1):
            for inputs, target in zip(inputs_set, targets_set):
                self.feed_forward(inputs)
                self.backpropagate(target, learning_rate)

            if time() > old_epoch_time + 0.2:
                old_epoch_time = time()
                print(f"Done with epoch {epoch}.", end='\r')
        print(f"Done training, {epochs} epochs done.")
