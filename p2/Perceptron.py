from typing import List


class Perceptron:
    """Perceptron unit"""
    weights: [float]
    bias: float
    threshold: float

    def __init__(self, weights: List[float], bias: float, threshold: float = .0):
        """
        Give a list of weights and a bias,
        threshold should not be used
        """
        self.weights = weights
        self.bias = bias
        self.threshold = threshold

    def _step_function(self, value: float) -> float:
        """These perceptrons use the step-function as activation-function"""
        return float(value >= self.threshold)

    def get_output(self, inputs: List[float]) -> float:
        """Processes the inputs with these weights and bias and applies the activation-function"""
        if len(inputs) != len(self.weights):
            raise ValueError(f"The inputs({inputs})-shape doesn't match the weights-shape: ({len(self.weights)},)")

        s = sum([input_value * weight for input_value, weight in zip(inputs, self.weights)]) + self.bias
        return self._step_function(s)

    def update(self, target: float, inputs: List[float], learning_rate: float) -> None:
        """Implements the perceptron learning rule to update this perceptron"""
        prediction = self.get_output(inputs)
        error = target - prediction
        for i in range(len(self.weights)):
            self.weights[i] += learning_rate * error * inputs[i]
        self.bias += learning_rate * error

    def __str__(self) -> str:
        return f"<Perceptron weights:{self.weights} bias:{self.bias}>"

    def __repr__(self):
        return self.__str__()
