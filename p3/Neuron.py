import math
from typing import List


def sigmoid(x: float) -> float:
    return 1 / (1 + math.e ** (-x))


class Neuron:
    """Neuron unit"""
    weights: [float]
    bias: float

    def __init__(self, weights: List[float], bias: float):
        self.weights = weights
        self.bias = bias

    def get_output(self, inputs: List[float]) -> float:
        """Processes the inputs with these weights and bias and applies the activation-function"""
        if len(inputs) != len(self.weights):
            raise ValueError(f"The inputs({inputs})-shape doesn't match the weights-shape: ({len(self.weights)},)")

        s = sum([input_value * weight for input_value, weight in zip(inputs, self.weights)]) + self.bias
        return sigmoid(s)

    def __str__(self) -> str:
        return f"<Perceptron weights:{self.weights} bias:{self.bias}>"

    def __repr__(self):
        return self.__str__()
