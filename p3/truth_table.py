from IPython.display import Markdown
from itertools import product


def truth_table_neuron(neuron) -> Markdown:
    """Creates a truth table of the given perceptron and prints it"""
    n_inputs = len(neuron.weights)
    output_string = f"|{'|'.join(map('in {}'.format, range(n_inputs)))}|out|\n|{'-|'*n_inputs}-|"
    for inputs in product((0, 1), repeat=n_inputs):
        output_string += f"\n|{'|'.join(map(str, inputs))}|{neuron.get_output(inputs)}|"
    return Markdown(output_string)
